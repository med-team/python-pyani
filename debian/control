Source: python-pyani
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all-dev,
               python3-setuptools,
               python3-biopython <!nocheck>,
               python3-intervaltree <!nocheck>,
               python3-jinja2 <!nocheck>,
               python3-matplotlib <!nocheck>,
               python3-networkx <!nocheck>,
               python3-pandas <!nocheck>,
               python3-scipy <!nocheck>,
               python3-seaborn <!nocheck>,
               python3-sqlalchemy <!nocheck>,
               python3-tqdm <!nocheck>,
               mummer <!nocheck>,
               ncbi-blast+ <!nocheck>,
               python3-pytest <!nocheck>
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/python-pyani
Vcs-Git: https://salsa.debian.org/med-team/python-pyani.git
Homepage: https://github.com/widdowquinn/pyani
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-python

Package: python3-pyani
Architecture: any
Section: python
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-biopython,
         python3-matplotlib,
         python3-pandas,
         python3-scipy,
         python3-seaborn,
         mummer,
         ncbi-blast+,
Description: Python3 module for average nucleotide identity analyses
 Pyani is a Python3 module and script that provides support for
 calculating average nucleotide identity (ANI) and related measures for
 whole genome comparisons, and rendering relevant graphical summary
 output. Where available, it takes advantage of multicore systems, and
 can integrate with SGE/OGE-type job schedulers for the sequence
 comparisons.
